import Component from '@glimmer/component';
import { action } from '@ember/object';
import { inject as service } from '@ember/service';

export default class ItemComponent extends Component {
    @service store;

    @action
    delete() {
        return this.args.data.destroyRecord();
    }
}
