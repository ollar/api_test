// import JSONAPIAdapter from '@ember-data/adapter/json-api';
import RestAdapter from '@ember-data/adapter/rest';

export default class ApplicationAdapter extends RestAdapter {
    host = 'http://localhost:8080';

    sortQueryParams = false;

    query(store, type, query) {
        let url = this.buildURL(type.modelName, null, null, 'query', query);

        if (query.find) {
            url += `?find=${encodeURIComponent(JSON.stringify(query.find))}`;
        }
        if (query.sort) {
            url += `&sort=${encodeURIComponent(JSON.stringify(query.sort))}`;
        }
        if (query.page) {
            url += `&page=${query.page}`;
        }

        return this.ajax(url, 'GET');
    }
}
