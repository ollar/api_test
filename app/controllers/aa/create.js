import Controller from '@ember/controller';

export default Controller.extend({
    actions: {
        onSubmit() {
            this.model.save();
        }
    }
});
