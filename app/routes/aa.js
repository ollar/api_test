import Route from '@ember/routing/route';

export default Route.extend({
    model() {
        // return this.store.findAll('aa');
        return this.store.query('aa', {
            find: {
                age: {
                    $gte: 11
                },
            },
            sort: {
                age: 1,
                name: -1
            },
            page: 1
        });
    }
});
